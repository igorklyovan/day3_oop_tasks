import './breed.dart';


class Bulldog implements Breed{
  @override
  String breedName;

  @override
  String color;
  bool earsCut;

  Bulldog(this.breedName, this.color, this.earsCut);
}