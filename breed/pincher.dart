import 'package:flutter/cupertino.dart';

import 'breed.dart';

class Pincher implements Breed {
  @override
  String color;
  @override
  String breedName;
  double size;

  Pincher({this.color, @required this.breedName, this.size});
}

void main() {
  Pincher pincher = Pincher(color: 'black', breedName: 'pincher', size: 20.5);
  print(pincher.size);
}
