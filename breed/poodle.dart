import './breed.dart';

class Poodle implements Breed {
  @override
  String breedName;
  @override
  String color;
  bool tailCut;

  Poodle(this.breedName, this.color, [this.tailCut]);
}
