
import './request.dart';

class SubRequest extends Request{

  SubRequest(String url) : super(url);

  @override
  get type => 'http';
}

void main() {
  SubRequest subRequest = SubRequest('google.com');
  print(subRequest.url);
  print(subRequest.type);
}