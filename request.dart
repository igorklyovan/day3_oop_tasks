
class Request {
  String _url;
  String _type = 'https';

  Request(this._url);

  String get type => _type;

  String get url => _url;
}