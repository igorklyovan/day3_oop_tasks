int sumInt(int a, int b) => a + b;

double sumDouble(double a, double b) => a + b;

double discriminant(double a, double b, double c) => b*b - 4*a*c;
